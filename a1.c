#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>

int is_dir( char path[])
{
	struct stat path_stat;
	stat(path, &path_stat);
	return S_ISDIR(path_stat.st_mode);
}
int check_file_consistency(const char path[]) {
	int result = 1;
	int fd = open(path, O_RDWR);
	if (fd >= 0) {

		char head1[40] = "0";
		int er_magic = 0, er_version = 0, er_sect_nr = 0, er_sect_types = 0;
		//if everything is ok
		read(fd, head1, 6);
		char magic[3] = "";
		strncpy(magic, head1, 2);
		//
		// int magic_nr=
		int number = (int) strtol(magic, NULL, 16);
		int nr_sections = (int) head1[5];
		int version = (int) head1[4];
		if (number != 199) {
			er_magic = 1;
		}


		if (version < 41 || version > 83) {
			er_version = 1;
		}
		if (nr_sections < 2 || nr_sections > 14) {
			er_sect_nr = 1;
		}
		if (er_magic || er_sect_nr || er_sect_types || er_version) {

			result = 0;

		}


		for (int i = 1; i <= nr_sections; i++) {

			read(fd, head1, 11);
			char section_type;
			read(fd, &section_type, 1);

			int offset = 0;
			read(fd, &offset, 4);
			int size = 0;
			read(fd, &size, 4);
			if (section_type != 14 && section_type != 11 && section_type != 39 && section_type != 88 &&
				section_type != 43) {
				result = 0;


			}


		}
		close(fd);
	} else {
		perror("File not found");
		exit(2);
		result = 0;
	}

	return result;

}
int file_ok(char const path[])
	{

	int fd = open(path, O_RDWR);
	if (fd >= 0) {
		char t[200];
		read(fd,t,200);
		lseek(fd, 5, SEEK_SET);
		char nr_sections = 0;

		read(fd, &nr_sections, 1);
		int offset = 0;
		int size = 0;
		for (int i = 1; i <= nr_sections; i++) {
			//  lseek(fd, 6, SEEK_SET);
			lseek(fd,6+20*(i-1),SEEK_SET);//start new section
			char head1[12];//the name
			read(fd, head1, 11);
			char section_type;
			read(fd, &section_type, 1);


			read(fd, &offset, 4);
			read(fd, &size, 4);
			lseek(fd,offset,SEEK_SET);


		
		if (size <= 0) {
			printf("Invalid section");
			exit(2);
		}
		int total_lines = 1;
		int read_bytes = 0;

		while (read_bytes < size) {
			char aux;
			read(fd, &aux, 1);
			//printf("%c",aux);
			if (aux == '\n') {
				total_lines++;
			}
			read_bytes++;
		}
			//printf("%d %s\n", total_lines,path);
		if(total_lines>16) {
			close(fd);
			return 1;
		}






}

		close(fd);
	}
	return 0;
	

}

int get_size(const char file_path[])
{

	int size = 0;
	int fp = open(file_path, O_RDONLY);
	if (fp > 0)
	{
		char c;
		while (read(fp, &c, 1))
			size++;
		close(fp);
		return size;

	}
	else
	{
		perror("Invalid file");
		return -1;
	}
}
int get_dir_size(const char path[])
{
	DIR *dir;
	int size = 0;
	if ((dir = opendir(path) ) != NULL)
	{
		struct dirent *ent;

		

			while ((ent = readdir (dir)) != NULL)
			{
				int aux = (strstr(ent->d_name, ".") == ent->d_name);
				if (!aux)
				{
					char t[100];					//absolute file path
					strcpy(t, path);
					strcat(t, "/");
					strcat(t, ent->d_name);
					if (is_dir(t))
					{
						int n = get_dir_size(t);
						size += n;
					}
					else
					{
						int n = get_size(t);
						size += n;
					}




				}
			}
			closedir(dir);
		}
		
	

	return size;
}
void print_found(const char dir_path[], const char before_path[])
{

	DIR *dir;

	char path[100] = "";
	strcpy(path, dir_path);
	//  path[sizeof(dir_path)]='y';
	struct dirent *ent;

	if ((dir = opendir(path) ) != NULL)
	{


		while ((ent = readdir (dir)) != NULL)
		{

			char t[100];					//absolute file path
			strcpy(t, dir_path);
			strcat(t, "/");
			strcat(t, ent->d_name);

			int n = is_dir(t);
			char relative_path[100];				//relative file path
			strcpy(relative_path, before_path);
			strcat(relative_path, ent->d_name);
			if (!n) { 	//is a file
				n = 1; //chek conditions
				

					n=n * file_ok(t)*check_file_consistency(t);
				
				

				if (n)
				{
					printf("\n%s", t);
				}
			}


		}
		closedir (dir);

		dir = opendir(dir_path);
		while ((ent = readdir (dir)) != NULL)
		{
			int aux = (strstr(ent->d_name, ".") == ent->d_name);
			if (!aux)
			{
				char t[100];
				strcpy(t, dir_path);
				strcat(t, "/");
				strcat(t, ent->d_name);


				int n = is_dir(t);
				char relative_path[100];
				strcpy(relative_path, before_path);
				strcat(relative_path, ent->d_name);
				strcat(relative_path, "/");
				
				if (n)
				{
					
					
						print_found(t , relative_path);
					

				}



			}

		}
		closedir(dir);
	}
	else
	{
		/* could not open directory */
		perror ("Could not open directory");
	}

}

void print_all(const char dir_path[], const char before_path[], int name_constraint, const char beggining[], int size_constraint, int min_size, int reccursive)
{

	DIR *dir;

	char path[100] = "";
	strcpy(path, dir_path);
	//  path[sizeof(dir_path)]='y';
	struct dirent *ent;

	if ((dir = opendir(path) ) != NULL)
	{


		while ((ent = readdir (dir)) != NULL)
		{

			char t[100];					//absolute file path
			strcpy(t, dir_path);
			strcat(t, "/");
			strcat(t, ent->d_name);

			int n = is_dir(t);
			char relative_path[100];				//relative file path
			strcpy(relative_path, before_path);
			strcat(relative_path, ent->d_name);
			if (!n) { 	//is a file
				n = 1; //chek conditions
				if (name_constraint)
				{
					n = n * (strstr(ent->d_name, beggining) == ent->d_name);
				}
				
				if (size_constraint)
				{
					int size = get_size(t);

					n = n * (size > min_size);
				}

				if (n)
				{
					printf("%s\n", t);
				}
			}


		}
		closedir (dir);

		dir = opendir(dir_path);
		while ((ent = readdir (dir)) != NULL)
		{
			int aux = (strstr(ent->d_name, ".") == ent->d_name);
			if (!aux)
			{
				char t[100];
				strcpy(t, dir_path);
				strcat(t, "/");
				strcat(t, ent->d_name);


				int n = is_dir(t);
				char relative_path[100];
				strcpy(relative_path, before_path);
				strcat(relative_path, ent->d_name);
				strcat(relative_path, "/");
				if (name_constraint)
				{
					n = n * (strstr(ent->d_name, beggining) == ent->d_name);
				}
				if (size_constraint)
				{
					int size = get_dir_size(t);

					n = n * (size > min_size);
				}
                int print_dir=!(size_constraint);

				if (n)
				{
                    if(print_dir)
                    {
                        printf("%s\n", t );
                    }
					if (reccursive)
					{
						print_all(t , relative_path, name_constraint, beggining, size_constraint, min_size, reccursive);
					}

				}



			}

		}
		closedir(dir);
	}
	else
	{
		/* could not open directory */
		perror ("Could not open directory");
	}

}
int check_file(const char path[])
{
	int result = 1;
	int fd = open(path, O_RDWR);
	if (fd >= 0) {

		char head1[40] = "0";
		int er_magic = 0, er_version = 0, er_sect_nr = 0, er_sect_types = 0;
		//if everything is ok
		read(fd, head1, 6);
		char magic[3] = "";
		strncpy(magic, head1, 2);
		//
		// int magic_nr=
		int number = (int) strtol(magic, NULL, 16);
		int nr_sections = (int) head1[5];
		int version = (int) head1[4];
		if (number != 199) {
			er_magic = 1;
		}


		if (version < 41 || version > 83) {
			er_version = 1;
		}
		if (nr_sections < 2 || nr_sections > 14) {
			er_sect_nr = 1;
		}
		if (er_magic || er_sect_nr || er_sect_types || er_version) {
			printf("ERROR\n");
			result = 0;

		}
		if (er_magic) {
			printf("wrong magic");
		} else if (er_version) {
			printf("wrong version");

		} else if (er_sect_nr) {
			printf("wrong sect_nr");
		} else {


			for (int i = 1; i <= nr_sections; i++) {

				read(fd, head1, 11);
				char section_type;
				read(fd, &section_type, 1);

				int offset = 0;
				read(fd, &offset, 4);
				int size = 0;
				read(fd, &size, 4);
				if (section_type != 14 && section_type != 11 && section_type != 39 && section_type != 88 &&
				        section_type != 43) {
					result = 0;
					printf("ERROR\nwrong sect_types");

				}


			}

		}
		close(fd);
	}
	else
	{
		perror("File not found");
		exit(2);
		result = 0;
	}

	return result;

}
void parse_file(const char path[]) {
	if (check_file(path)) {
		int fd = open(path, O_RDWR);
		if (fd >= 0) {

			char head1[40] = "0";


			read(fd, head1, 6);
			char magic[3] = "";
			strncpy(magic, head1, 2);
			

			int nr_sections = (int) head1[5];
			int version = (int) head1[4];


			printf("SUCCESS\n");
			printf("version=%d\n", version);
			printf("nr_sections=%d\n", nr_sections);


			for (int i = 1; i <= nr_sections; i++) {
				//  lseek(fd, 6, SEEK_SET);
				read(fd, head1, 11);
				char section_type;
				read(fd, &section_type, 1);
				printf("section%d: %s %d ", i, head1, section_type);
				int offset = 0;
				read(fd, &offset, 4);
				int size = 0;
				read(fd, &size, 4);
				printf("%d\n", size);


			}



			close(fd);
		}
		else {
			perror("File not found");
			exit(2);
		}
	}
}

void extract_line(const char path[], int section, int line)
{

	int fd = open(path, O_RDWR);
	if (fd >= 0) {
		char t[200];
		read(fd, t, 200);

		lseek(fd, 5, SEEK_SET);
		char nr_sections = 0;
		read(fd, &nr_sections, 1);
		if (section > nr_sections) {
			printf("ERROR\nInvalid section");
			exit(2);
		}
		int offset = 0;
		int size = 0;
		for (int i = 1; i <= section; i++) {
			//  lseek(fd, 6, SEEK_SET);
			char head1[12];//the name
			read(fd, head1, 11);
			char section_type;
			read(fd, &section_type, 1);


			read(fd, &offset, 4);

			read(fd, &size, 4);
		}
		if (size <= 0) {
			printf("Invalid section");
			exit(2);
		}
		lseek(fd, offset, SEEK_SET);

		int total_lines = 1;
		int read_bytes = 0;
		char aux1;
		while (read_bytes < size&&(read(fd, &aux1, 1)==1)) {
			
			
			//printf("%c",aux);
			if (aux1 == '\n') {
				total_lines++;
			}
			read_bytes++;
			
		}
		if (line > total_lines)
		{
			printf("ERROR\nLine exceedes");
			exit(2);
		}
		//printf("\n\n");
		read_bytes = 0;
		lseek(fd, offset, SEEK_SET);
		for (int i = 1; i <= total_lines - line; i++)
		{
			char c = '\0';
			while (c != '\n')
			{

				read(fd, &c, 1);
				read_bytes++;
				//printf("%c",c);

			}


		}

		char c = '\123';
		// printf("\n");
		char aux[5000000] = "";
		int line_size = 0;
		while (c != '\n' && (read_bytes <= size)) {
			read(fd, &c, 1);
			read_bytes++;
			//printf("%c",c);
			aux[line_size] = c;
			line_size++;
		}
		printf("SUCCESS\n");
		if(aux[line_size-1]=='\0')
		{
			line_size--;
		}
		for (int i = line_size - 1; i > -1; i--)
		{
			if (aux[i] != '\n')
				printf("%c", aux[i]);
		}





	}
	else {
		printf("ERROR\nInvalid file");
		exit(2);
	}


}

int main(int argc, char **argv) {
	if (argc >= 2) {
		if (strstr(argv[1], "variant")) {
			printf("60268\n");
		}
	}
	char size_threshold[400];
	int list = 0, recursive = 0, name_constraint = 0, size_constraint = 0, parse = 0;
	char name_beggining[100] = "";
	int extract = 0;
	int section = 0, line = 0;
	char aux[300];
	char path[400];
	int find_all = 0;
	int min_size = 0;

	for (int i = 1; i < argc; i++)
	{
		if (strstr(argv[i], "list"))
		{
			list = 1;
		}


		if (strstr(argv[i], "recursive"))
		{

			recursive = 1;
		}
		if (strstr(argv[i], "path"))
		{
			strncpy(path, argv[i] + 5, sizeof(path));

		}
		if (strstr(argv[i], "name_starts_with"))
		{
			strncpy(name_beggining, argv[i] + 17, sizeof(name_beggining));
			name_constraint = 1;
		}
		if (strstr(argv[i], "size_greater"))
		{
			strncpy(size_threshold, argv[i] + 13, sizeof(size_threshold));
			size_constraint = 1;
			min_size = atoi(size_threshold);

		}
		if (strstr(argv[i], "parse"))
		{
			parse = 1;
		}
		if (strstr(argv[i], "extract"))
		{
			extract = 1;;
		}
		if (strstr(argv[i], "section="))
		{
			strcpy(aux, argv[i] + 8);

			section = atoi(aux);
		}
		if (strstr(argv[i], "line="))
		{
			strcpy(aux, argv[i] + 5);

			line = atoi(aux);
		}
		if (strstr(argv[i], "findall"))
		{
			find_all = 1;
		}



	}
	char absolute_path[1024];
	getcwd(absolute_path, sizeof(absolute_path));

	strcat(absolute_path, "/");
	strcat(absolute_path, path);
	if(list+parse+extract+find_all>1)
	{
		printf("ERROR");
		exit(-1);
	}
	else
	if (list)
	{
		if (!is_dir(absolute_path))
			perror("ERROR\ninvalid directory path");
		else
			printf("SUCCESS\n");

		print_all(path, "", name_constraint, name_beggining, size_constraint, min_size, recursive);


	}
	else if (parse)
	{

		parse_file(path);

	}
	else if (extract)
	{

		extract_line(path, section, line);
	}
	else if(find_all)
	{

		DIR *d=opendir(absolute_path);
		if(d!=NULL)
		{
			printf("SUCCESS");
			closedir(d);
			print_found(path,"");
		}
		else
		{
			printf("ERROR\ninvalid path");
		}

	}







	return 0;

}